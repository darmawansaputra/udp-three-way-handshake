// ClientU.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <string>
#include <WS2tcpip.h>

#pragma comment(lib, "ws2_32.lib")

//Definisi umum
const int maxLength = 5000;
sockaddr_in server;
int s_len = sizeof(server);

//Definisi type enum
enum Type {
	SYN, SYNACK, ACK, FIN, FINACK, DATA
};

//Definisi struct data
struct Data {
	int stats;
	int num;
};

void SendPacket(SOCKET &sock, int data_size, char* buffer, Data &data) {
	memcpy(buffer, &data, sizeof(data));
	sendto(sock, buffer, data_size, 0, (SOCKADDR *)&server, s_len);
}

int main() {
	//Inisialisasi
	Data data;
	Type shake = Type::SYN;
	bool waiting = false;
	int c_counter = 0;
	const int data_size = sizeof(data);
	char buffer[data_size];

	WSAData wsData;
	WSAStartup(MAKEWORD(2, 2), &wsData);

	//Create socket
	SOCKET sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);

	//Server identification
	server.sin_family = AF_INET;
	server.sin_port = htons(8888);
	inet_pton(AF_INET, "127.0.0.1", &server.sin_addr);

	printf("=== CLIENT ===\n\n");

	int a;
	//Looping receive & send data
	while (true) {
		int _recv;
		memset(buffer, '\0', data_size);

		//When send data
		if (shake != Type::DATA && !waiting) {
			if (shake == Type::SYN) {
				printf("Client mengirim SYN\n");
				data.stats = Type::SYN;
			}
			else if (shake == Type::ACK) {
				printf("Client mengirim ACK\n");
				data.stats = Type::ACK;
				shake = Type::DATA;
			}

			data.num = 0;

			SendPacket(sock, data_size, buffer, data);

			waiting = true;
		}

		//When receive data
		if (_recv = recvfrom(sock, buffer, data_size, 0, (SOCKADDR *)&server, &s_len)) {
			memcpy(&data, buffer, sizeof(data));

			if (data.stats == Type::SYN) {
				
			}
			else if (data.stats == Type::SYNACK) {
				shake = Type::ACK;
				printf("Client menerima SYNACK\n");

				waiting = false;
			}
			else if (data.stats == Type::ACK) {

			}
			else if (data.stats == Type::FIN) {

			}
			else if (data.stats == Type::FINACK) {
				printf("Client menerima FINACK\n");
				printf("Client mengirim ACK\n");

				data.stats = Type::ACK;
				data.num = 0;

				SendPacket(sock, data_size, buffer, data);
			}
			else if (data.stats == Type::DATA) {
				printf("Client menerima DATA: %d\n", data.num);

				if (data.num < maxLength) {
					data.stats = Type::DATA;
					data.num = 10;

					for (int i = 0; i < 5; i++)
						SendPacket(sock, data_size, buffer, data);
				}
				else if (data.num == maxLength) {
					printf("Client mengirim FIN\n");
					
					data.stats = Type::FIN;
					data.num = 0;

					SendPacket(sock, data_size, buffer, data);
				}
				
			}
		}
	}

	//Clean socket
	closesocket(sock);
	WSACleanup();

	return 0;
}

