// ServerU.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <stdio.h>
#include <string>
#include <WS2tcpip.h>

#pragma comment(lib, "ws2_32.lib")

//Definisi umum
const int maxLength = 5000;
sockaddr_in server, client;
int c_len = sizeof(client);

//Definisi type enum
enum Type {
	SYN, SYNACK, ACK, FIN, FINACK, DATA
};

//Definisi struct data
struct Data {
	int stats;
	int num;
};

void SendPacket(SOCKET &sock, int data_size, char* buffer, Data &data) {
	memcpy(buffer, &data, sizeof(data));
	sendto(sock, buffer, data_size, 0, (SOCKADDR *)&client, c_len);
}

int main() {
	//Inisialisasi
	Data data;
	Type shake = Type::SYN;
	int c_counter = 0;
	const int data_size = sizeof(data);
	char buffer[data_size];

	WSAData wsData;
	WSAStartup(MAKEWORD(2, 2), &wsData);

	//Create socket
	SOCKET sock = socket(AF_INET, SOCK_DGRAM, 0);

	//Server identification
	server.sin_family = AF_INET;
	server.sin_port = htons(8888);
	server.sin_addr.S_un.S_addr = INADDR_ANY;

	//Bind data
	bind(sock, (SOCKADDR *)&server, sizeof(server));

	printf("=== SERVER ===\n\n");

	//Looping receive & send data
	while (true) {
		int _recv;
		memset(buffer, '\0', sizeof(data_size));

		//When receive data
		if (_recv = recvfrom(sock, buffer, data_size, 0, (SOCKADDR *)&client, &c_len)) {
			memcpy(&data, buffer, sizeof(data));

			if (data.stats == Type::SYN) {
				printf("Server menerima SYN\n");

				shake = Type::SYNACK;

				//Send SYNACK to client
				data.stats = Type::SYNACK;
				data.num = 0;
				
				printf("Server mengirim SYNACK\n");
				SendPacket(sock, data_size, buffer, data);
			}
			else if (data.stats == Type::ACK) {
				printf("Server menerima qqACK\n");

				if (shake == Type::ACK)
					continue;

				shake = Type::ACK;

				data.stats = Type::DATA;
				data.num = c_counter;

				printf("Server mengirim DATA: %d\n", c_counter);
				SendPacket(sock, data_size, buffer, data);
			}
			else if (data.stats == Type::FIN) {
				printf("Server menerima FIN\n");
				
				data.stats = Type::FINACK;
				data.num = 0;

				printf("Server mengirim FINACK\n");
				SendPacket(sock, data_size, buffer, data);
			}
			else if (data.stats == Type::FINACK) {

			}
			else if (data.stats == Type::DATA) {
				printf("Server menerima DATA: %d\n", data.num);
				c_counter += data.num;

				if (c_counter % 50 == 0 && c_counter <= maxLength) {
					data.stats = Type::DATA;
					data.num = c_counter;

					printf("Server mengirim DATA: %d\n", c_counter);
					SendPacket(sock, data_size, buffer, data);
				}
			}
		}
	}

	//Clean socket
	closesocket(sock);
	WSACleanup();

    return 0;
}

